"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _guide = require("./util/guide.util");

var _guide2 = _interopRequireDefault(require("./guide.facade"));

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

class GuideFactory {
  static createGuideManagerWithGuideSchemes(reactComponent, guidesSchemes, guideRepository) {
    const guides = (0, _guide.mapGuideSchemesToReactComponents)(guidesSchemes);
    return new _guide2.default(reactComponent, guides, guideRepository);
  }

  static createGuideManager(reactComponents, guides, guideRepository) {
    return new _guide2.default(reactComponents, guides, guideRepository);
  }

}

exports.default = GuideFactory;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideFactory, "GuideFactory", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\guide.factory.js");
  leaveModule(module);
})();

;