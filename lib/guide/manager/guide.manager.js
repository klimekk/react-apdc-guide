"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _guide = require("../util/guide.util");

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

class GuideManager {
  constructor(reactComponent, guides, guideRepository) {
    this.reactComponent = reactComponent;
    this.guideRepository = guideRepository;
    this.versions = [];
    this.spotlights = [];
    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.finish = this.finish.bind(this);
    this.remindLater = this.remindLater.bind(this);
    this.functions = {
      next: this.next,
      finish: this.finish,
      back: this.back,
      remindLater: this.remindLater
    };
    this.setup(guides);
  }

  setup(guides) {
    this.initializeActiveSpotlightsState();
    return this.guideRepository.getAllOfVersionsOfSeenGuides().then(seenGuideVersions => {
      const unseenGuides = (0, _guide.getAllUnseenGuides)(seenGuideVersions, guides);
      const spotlights = unseenGuides.spotlights,
            versions = unseenGuides.versions;
      this.spotlights = this.addMethodsToSpotlights(spotlights);
      this.versions = versions;
      this.setSpotlightNumberOnStore((0, _guide.getInitValue)(spotlights));
      return seenGuideVersions;
    }).catch(console.warn);
  }

  isStateNoInitialized() {
    return !this.reactComponent.state;
  }

  initializeActiveSpotlightsState() {
    const value = null;
    const reactComponent = this.reactComponent;

    if (this.isStateNoInitialized()) {
      reactComponent.state = {
        activeSpotlight: value
      };
      return;
    }

    reactComponent.setState(state => (0, _objectSpread2.default)({}, state, {
      activeSpotlight: value
    }));
  }

  setSpotlightNumberOnStore(initValue) {
    this.reactComponent.setState(state => (0, _objectSpread2.default)({}, state, {
      activeSpotlight: initValue
    }));
  }

  next() {
    return this.reactComponent.setState(state => (0, _objectSpread2.default)({}, state, {
      activeSpotlight: state.activeSpotlight + 1
    }));
  }

  back() {
    this.reactComponent.setState(state => (0, _objectSpread2.default)({}, state, {
      activeSpotlight: state.activeSpotlight - 1
    }));
  }

  finish() {
    this.reactComponent.setState(state => (0, _objectSpread2.default)({}, state, {
      activeSpotlight: null
    }));
    this.guideRepository.saveVersionsOfSeenGuides(this.versions);
  }

  remindLater() {
    this.reactComponent.setState(state => (0, _objectSpread2.default)({}, state, {
      activeSpotlight: null
    }));
  }

  renderSpotlight() {
    return this.spotlights[this.reactComponent.state.activeSpotlight];
  }

  mapStrToFunctions(actions) {
    return actions.map(action => {
      const onClick = this.functions[action.onClick];

      if (!onClick) {
        console.error(`Unrecognized guide function by name: ${action.onClick}.`);
      }

      return (0, _objectSpread2.default)({}, action, {
        onClick
      });
    });
  }

  addMethodsToSpotlights(spotlights) {
    return spotlights.map(spotlight => (0, _objectSpread2.default)({}, spotlight, {
      props: (0, _objectSpread2.default)({}, spotlight.props, {
        actions: this.mapStrToFunctions(spotlight.props.actions)
      })
    }));
  }

  // @ts-ignore
  __reactstandin__regenerateByEval(key, code) {
    // @ts-ignore
    this[key] = eval(code);
  }

}

exports.default = GuideManager;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideManager, "GuideManager", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\manager\\guide.manager.js");
  leaveModule(module);
})();

;