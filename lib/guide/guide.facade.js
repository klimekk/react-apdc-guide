"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _onboarding = require("@atlaskit/onboarding");

var _guide = _interopRequireDefault(require("./manager/guide.manager"));

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

class GuideFacade {
  constructor(reactComponent, guides, guideRepository) {
    this.guideManager = new _guide.default(reactComponent, guides, guideRepository);
  }

  renderGuideTour() {
    return _react.default.createElement(_onboarding.SpotlightTransition, null, this.guideManager.renderSpotlight());
  }

  // @ts-ignore
  __reactstandin__regenerateByEval(key, code) {
    // @ts-ignore
    this[key] = eval(code);
  }

}

exports.default = GuideFacade;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideFacade, "GuideFacade", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\guide.facade.jsx");
  leaveModule(module);
})();

;