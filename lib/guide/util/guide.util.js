"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertLastSpotlightFinishMethodToNext = convertLastSpotlightFinishMethodToNext;
exports.convertFirstSpotlightFinishMethodToBack = convertFirstSpotlightFinishMethodToBack;
exports.isGuideWasNotSeen = isGuideWasNotSeen;
exports.getAllUnseenGuides = getAllUnseenGuides;
exports.getInitValue = getInitValue;
exports.mapGuideSchemesToReactComponents = mapGuideSchemesToReactComponents;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

require("core-js/modules/web.dom.iterable");

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _react = _interopRequireDefault(require("react"));

var _shortid = _interopRequireDefault(require("shortid"));

var _guideSpotlight = _interopRequireDefault(require("../components/guideSpotlight.component"));

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

const ACTIONS = {
  close: {
    onClick: 'finish',
    text: 'Finish'
  },
  next: {
    onClick: 'next',
    text: 'Next'
  },
  back: {
    onClick: 'back',
    text: 'Back'
  },
  skip: {
    onClick: 'finish',
    text: 'Skip'
  },
  remindLater: {
    onClick: 'remindLater',
    text: 'Remind later'
  }
};

function deleteLastSpotlight(spotlights) {
  return spotlights.slice(0, spotlights.length - 1);
}

function filterFinishMethods(actions) {
  return actions.filter((_ref) => {
    let onClick = _ref.onClick;
    return !['finish', 'close', 'remindLater'].some(e => e === onClick);
  });
}

function isNextMethodExist(actions) {
  return actions.some((_ref2) => {
    let onClick = _ref2.onClick;
    return onClick === 'next';
  });
}

function isBackMethodExist(actions) {
  return actions.some((_ref3) => {
    let onClick = _ref3.onClick;
    return onClick === 'back';
  });
}

function addNextMethod(actions) {
  const nextAction = ACTIONS.next;

  if (isNextMethodExist(actions)) {
    return [];
  }

  return [nextAction];
}

function addBackMethod(actions) {
  const backAction = ACTIONS.back;

  if (isBackMethodExist(actions)) {
    return [];
  }

  return [backAction];
}

function convertSpotlightFinishToNextMethod(spotlights) {
  const spotlight = spotlights[0];

  if (spotlights.length === 1) {
    return (0, _objectSpread2.default)({}, spotlight, {
      props: (0, _objectSpread2.default)({}, spotlight.props, {
        actions: [...spotlight.props.actions, ...addNextMethod(spotlight.props.actions)]
      })
    });
  }

  return (0, _objectSpread2.default)({}, spotlight, {
    props: (0, _objectSpread2.default)({}, spotlight.props, {
      actions: [...filterFinishMethods(spotlight.props.actions), ...addNextMethod(spotlight.props.actions)]
    })
  });
}

function convertLastSpotlightFinishMethodToNext(spotlights) {
  return [...deleteLastSpotlight(spotlights), convertSpotlightFinishToNextMethod(spotlights)];
}

function convertSpotlightFinishToBackMethod(spotlight) {
  return (0, _objectSpread2.default)({}, spotlight, {
    props: (0, _objectSpread2.default)({}, spotlight.props, {
      actions: [...filterFinishMethods(spotlight.props.actions), ...addBackMethod(spotlight.props.actions)]
    })
  });
}

function deleteFirstSpotlight(spotlights) {
  return spotlights.slice(1, spotlights.length);
}

function convertFirstSpotlightFinishMethodToBack(spotlights) {
  return [convertSpotlightFinishToBackMethod(spotlights[0]), ...deleteFirstSpotlight(spotlights)];
}

function isGuideWasNotSeen(seenGuidesVersions, guide) {
  return !seenGuidesVersions.some(v => v === guide.version);
}

function getAllUnseenGuides(seenGuidesVersions, guides) {
  const mergedSpotlights = [];
  const versions = [];
  guides.forEach((guide, index) => {
    if (isGuideWasNotSeen(seenGuidesVersions, guide)) {
      let spotlights = guide.spotlights;
      const version = guide.version;
      versions.push(version);
      const isNotLastGuide = guides.length - 1 > index;
      const isNotFirstGuide = index > 0;

      if (isNotLastGuide) {
        spotlights = convertLastSpotlightFinishMethodToNext(spotlights);
      }

      if (isNotFirstGuide) {
        spotlights = convertFirstSpotlightFinishMethodToBack(spotlights);
      }

      mergedSpotlights.push(...spotlights);
    }
  });
  return {
    spotlights: mergedSpotlights,
    versions
  };
}

function getInitValue(spotlights) {
  return spotlights.length ? 0 : null;
}

function mapActionsNameToObjects(actions) {
  return actions.map(action => ACTIONS[action]);
}

function generateSpotlightKey() {
  return `spotlight-${_shortid.default.generate()}`;
}

function mapSpotlightSchemeJsonToReactComponents(spotlightSchemes) {
  return spotlightSchemes.map(scheme => {
    const mappedScheme = (0, _objectSpread2.default)({}, scheme, {
      actions: mapActionsNameToObjects(scheme.actions)
    });
    return _react.default.createElement(_guideSpotlight.default, (0, _extends2.default)({
      key: generateSpotlightKey()
    }, mappedScheme));
  });
}

function mapGuideSchemesToReactComponents(guideSchemes) {
  return guideSchemes.map(guidScheme => {
    const version = guidScheme.version,
          spotlightsSchemes = guidScheme.spotlightsSchemes;
    return {
      version,
      spotlights: mapSpotlightSchemeJsonToReactComponents(spotlightsSchemes)
    };
  });
}

;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ACTIONS, "ACTIONS", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(deleteLastSpotlight, "deleteLastSpotlight", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(filterFinishMethods, "filterFinishMethods", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isNextMethodExist, "isNextMethodExist", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isBackMethodExist, "isBackMethodExist", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(addNextMethod, "addNextMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(addBackMethod, "addBackMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertSpotlightFinishToNextMethod, "convertSpotlightFinishToNextMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertLastSpotlightFinishMethodToNext, "convertLastSpotlightFinishMethodToNext", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertSpotlightFinishToBackMethod, "convertSpotlightFinishToBackMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(deleteFirstSpotlight, "deleteFirstSpotlight", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertFirstSpotlightFinishMethodToBack, "convertFirstSpotlightFinishMethodToBack", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isGuideWasNotSeen, "isGuideWasNotSeen", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getAllUnseenGuides, "getAllUnseenGuides", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getInitValue, "getInitValue", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapActionsNameToObjects, "mapActionsNameToObjects", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(generateSpotlightKey, "generateSpotlightKey", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapSpotlightSchemeJsonToReactComponents, "mapSpotlightSchemeJsonToReactComponents", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapGuideSchemesToReactComponents, "mapGuideSchemesToReactComponents", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  leaveModule(module);
})();

;