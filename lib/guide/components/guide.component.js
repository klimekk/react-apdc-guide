"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _onboarding = require("@atlaskit/onboarding");

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

class Guide extends _react.default.Component {
  render() {
    return _react.default.createElement(_onboarding.SpotlightManager, null, this.props.children);
  }

  // @ts-ignore
  __reactstandin__regenerateByEval(key, code) {
    // @ts-ignore
    this[key] = eval(code);
  }

}

Guide.defaultProps = {
  children: _react.default.createElement(_react.default.Fragment, null)
};
Guide.propTypes = {
  children: _propTypes.default.element
};
const _default = Guide;
var _default2 = _default;
exports.default = _default2;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Guide, "Guide", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guide.component.jsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guide.component.jsx");
  leaveModule(module);
})();

;