"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _onboarding = require("@atlaskit/onboarding");

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

class GuideTarget extends _react.default.Component {
  render() {
    return _react.default.createElement(_onboarding.SpotlightTarget, {
      name: this.props.name
    }, this.props.children);
  }

  // @ts-ignore
  __reactstandin__regenerateByEval(key, code) {
    // @ts-ignore
    this[key] = eval(code);
  }

}

GuideTarget.defaultProps = {
  children: _react.default.createElement(_react.default.Fragment, null)
};
GuideTarget.propTypes = {
  name: _propTypes.default.string.isRequired,
  children: _propTypes.default.any
};
const _default = GuideTarget;
var _default2 = _default;
exports.default = _default2;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideTarget, "GuideTarget", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guideTarget.component.jsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guideTarget.component.jsx");
  leaveModule(module);
})();

;