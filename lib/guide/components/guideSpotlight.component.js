"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _onboarding = require("@atlaskit/onboarding");

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

class GuideSpotlight extends _react.default.Component {
  render() {
    const _this$props = this.props,
          target = _this$props.target,
          actions = _this$props.actions,
          header = _this$props.header,
          position = _this$props.position,
          content = _this$props.content,
          width = _this$props.width;
    return _react.default.createElement(_onboarding.Spotlight, {
      target: target,
      actions: actions,
      heading: header,
      width: width,
      dialogPlacement: position
    }, content);
  }

  // @ts-ignore
  __reactstandin__regenerateByEval(key, code) {
    // @ts-ignore
    this[key] = eval(code);
  }

}

GuideSpotlight.defaultProps = {
  actions: [],
  header: '',
  width: 400,
  position: 'bottom center'
};
GuideSpotlight.propTypes = {
  target: _propTypes.default.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  actions: _propTypes.default.array,
  header: _propTypes.default.string,
  position: _propTypes.default.string,
  content: _propTypes.default.string.isRequired,
  width: _propTypes.default.number
};
const _default = GuideSpotlight;
var _default2 = _default;
exports.default = _default2;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideSpotlight, "GuideSpotlight", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guideSpotlight.component.jsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guideSpotlight.component.jsx");
  leaveModule(module);
})();

;