import React from 'react';
import { SpotlightTransition } from '@atlaskit/onboarding';
import GuideManager from './manager/guide.manager';

export default class GuideFacade {
  constructor(reactComponent, guides, guideRepository) {
    this.guideManager = new GuideManager(
      reactComponent,
      guides,
      guideRepository,
    );
  }

  renderGuideTour() {
    return (
      <SpotlightTransition>
        {this.guideManager.renderSpotlight()}
      </SpotlightTransition>
    );
  }
}
