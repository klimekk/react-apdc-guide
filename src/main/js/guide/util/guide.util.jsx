import React from 'react';
import shortId from 'shortid';
import GuideSpotlight from '../components/guideSpotlight.component';

const ACTIONS = {
  close: {
    onClick: 'finish',
    text: 'Finish',
  },
  next: {
    onClick: 'next',
    text: 'Next',
  },
  back: {
    onClick: 'back',
    text: 'Back',
  },
  skip: {
    onClick: 'finish',
    text: 'Skip',
  },
  remindLater: {
    onClick: 'remindLater',
    text: 'Remind later',
  },
};

function deleteLastSpotlight(spotlights) {
  return spotlights.slice(0, spotlights.length - 1);
}

function filterFinishMethods(actions) {
  return actions.filter(
    ({ onClick }) =>
      !['finish', 'close', 'remindLater'].some(e => e === onClick),
  );
}

function isNextMethodExist(actions) {
  return actions.some(({ onClick }) => onClick === 'next');
}

function isBackMethodExist(actions) {
  return actions.some(({ onClick }) => onClick === 'back');
}

function addNextMethod(actions) {
  const nextAction = ACTIONS.next;

  if (isNextMethodExist(actions)) {
    return [];
  }

  return [nextAction];
}

function addBackMethod(actions) {
  const backAction = ACTIONS.back;

  if (isBackMethodExist(actions)) {
    return [];
  }

  return [backAction];
}

function convertSpotlightFinishToNextMethod(spotlights) {
  const spotlight = spotlights[0];

  if (spotlights.length === 1) {
    return {
      ...spotlight,
      props: {
        ...spotlight.props,
        actions: [
          ...spotlight.props.actions,
          ...addNextMethod(spotlight.props.actions),
        ],
      },
    };
  }

  return {
    ...spotlight,
    props: {
      ...spotlight.props,
      actions: [
        ...filterFinishMethods(spotlight.props.actions),
        ...addNextMethod(spotlight.props.actions),
      ],
    },
  };
}

export function convertLastSpotlightFinishMethodToNext(spotlights) {
  return [
    ...deleteLastSpotlight(spotlights),
    convertSpotlightFinishToNextMethod(spotlights),
  ];
}

function convertSpotlightFinishToBackMethod(spotlight) {
  return {
    ...spotlight,
    props: {
      ...spotlight.props,
      actions: [
        ...filterFinishMethods(spotlight.props.actions),
        ...addBackMethod(spotlight.props.actions),
      ],
    },
  };
}

function deleteFirstSpotlight(spotlights) {
  return spotlights.slice(1, spotlights.length);
}

export function convertFirstSpotlightFinishMethodToBack(spotlights) {
  return [
    convertSpotlightFinishToBackMethod(spotlights[0]),
    ...deleteFirstSpotlight(spotlights),
  ];
}


export function isGuideWasNotSeen(seenGuidesVersions, guide) {
  return !seenGuidesVersions.some(v => v === guide.version);
}

export function getAllUnseenGuides(seenGuidesVersions, guides) {
  const mergedSpotlights = [];
  const versions = [];

  guides.forEach((guide, index) => {
    if (isGuideWasNotSeen(seenGuidesVersions, guide)) {
      let { spotlights } = guide;
      const { version } = guide;

      versions.push(version);
      const isNotLastGuide = guides.length - 1 > index;
      const isNotFirstGuide = index > 0;

      if (isNotLastGuide) {
        spotlights = convertLastSpotlightFinishMethodToNext(spotlights);
      }

      if (isNotFirstGuide) {
        spotlights = convertFirstSpotlightFinishMethodToBack(spotlights);
      }

      mergedSpotlights.push(...spotlights);
    }
  });

  return {
    spotlights: mergedSpotlights,
    versions,
  };
}

export function getInitValue(spotlights) {
  return spotlights.length ? 0 : null;
}

function mapActionsNameToObjects(actions) {
  return actions.map(action => ACTIONS[action]);
}

function generateSpotlightKey() {
  return `spotlight-${shortId.generate()}`;
}

function mapSpotlightSchemeJsonToReactComponents(spotlightSchemes) {
  return spotlightSchemes.map(scheme => {
    const mappedScheme = {
      ...scheme,
      actions: mapActionsNameToObjects(scheme.actions),
    };

    return <GuideSpotlight key={generateSpotlightKey()} {...mappedScheme} />;
  });
}

export function mapGuideSchemesToReactComponents(guideSchemes) {
  return guideSchemes.map(guidScheme => {
    const { version, spotlightsSchemes } = guidScheme;

    return {
      version,
      spotlights: mapSpotlightSchemeJsonToReactComponents(spotlightsSchemes),
    };
  });
}
