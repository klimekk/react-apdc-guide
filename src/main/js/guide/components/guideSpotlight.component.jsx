import React from 'react';
import PropTypes from 'prop-types';
import { Spotlight } from '@atlaskit/onboarding';

class GuideSpotlight extends React.Component {
  render() {
    const { target, actions, header, position, content, width } = this.props;

    return (
      <Spotlight
        target={target}
        actions={actions}
        heading={header}
        width={width}
        dialogPlacement={position}
      >
        {content}
      </Spotlight>
    );
  }
}

GuideSpotlight.defaultProps = {
  actions: [],
  header: '',
  width: 400,
  position: 'bottom center',
};

GuideSpotlight.propTypes = {
  target: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  actions: PropTypes.array,
  header: PropTypes.string,
  position: PropTypes.string,
  content: PropTypes.string.isRequired,
  width: PropTypes.number,
};

export default GuideSpotlight;
