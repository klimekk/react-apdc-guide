import React from 'react';
import PropTypes from 'prop-types';
import { SpotlightTarget } from '@atlaskit/onboarding';


class GuideTarget extends React.Component {
    render() {
        return (
            <SpotlightTarget name={this.props.name}>
                {this.props.children}
            </SpotlightTarget>
        );
    }
}
GuideTarget.defaultProps = {
    children: <React.Fragment></React.Fragment>,
};

GuideTarget.propTypes = {
    name: PropTypes.string.isRequired,
    children: PropTypes.any,
};

export default GuideTarget;
