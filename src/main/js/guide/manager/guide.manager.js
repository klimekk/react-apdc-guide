import { getAllUnseenGuides, getInitValue } from '../util/guide.util';

export default class GuideManager {
  constructor(reactComponent, guides, guideRepository) {
    this.reactComponent = reactComponent;
    this.guideRepository = guideRepository;
    this.versions = [];
    this.spotlights = [];

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.finish = this.finish.bind(this);
    this.remindLater = this.remindLater.bind(this);

    this.functions = {
      next: this.next,
      finish: this.finish,
      back: this.back,
      remindLater: this.remindLater,
    };

    this.setup(guides);
  }

  setup(guides) {
    this.initializeActiveSpotlightsState();

    return this.guideRepository
      .getAllOfVersionsOfSeenGuides()
      .then(seenGuideVersions => {
        const unseenGuides = getAllUnseenGuides(seenGuideVersions, guides);
        const { spotlights, versions } = unseenGuides;

        this.spotlights = this.addMethodsToSpotlights(spotlights);
        this.versions = versions;

        this.setSpotlightNumberOnStore(getInitValue(spotlights));
        return seenGuideVersions;
      })
      .catch(console.warn);
  }

  isStateNoInitialized() {
    return !this.reactComponent.state;
  }

  initializeActiveSpotlightsState() {
    const value = null;
    const { reactComponent } = this;

    if (this.isStateNoInitialized()) {
      reactComponent.state = {
        activeSpotlight: value,
      };

      return;
    }

    reactComponent.setState(state => ({ ...state, activeSpotlight: value }));
  }

  setSpotlightNumberOnStore(initValue) {
    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: initValue,
    }));
  }

  next() {
    return this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: state.activeSpotlight + 1,
    }));
  }

  back() {
    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: state.activeSpotlight - 1,
    }));
  }

  finish() {
    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: null,
    }));

    this.guideRepository.saveVersionsOfSeenGuides(this.versions);
  }

  remindLater() {
    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: null,
    }));
  }

  renderSpotlight() {
    return this.spotlights[this.reactComponent.state.activeSpotlight];
  }

  mapStrToFunctions(actions) {
    return actions.map(action => {
      const onClick = this.functions[action.onClick];

      if (!onClick) {
        console.error(
          `Unrecognized guide function by name: ${action.onClick}.`,
        );
      }

      return {
        ...action,
        onClick,
      };
    });
  }

  addMethodsToSpotlights(spotlights) {
    return spotlights.map(spotlight => ({
      ...spotlight,
      props: {
        ...spotlight.props,
        actions: this.mapStrToFunctions(spotlight.props.actions),
      },
    }));
  }
}
