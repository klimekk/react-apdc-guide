const JSDOMEnvironment = require('jest-environment-jsdom');
const fs = require('fs');
const { Script } = require('vm');

class MyEnvironment extends JSDOMEnvironment {
  loadScript(path) {
    const code = fs.readFileSync(path);
    const script = new Script(code);
    this.dom.runVMScript(script);
  }
}

module.exports = MyEnvironment;
