export default class GuideRepositoryMock {
  constructor(seenVersions = []) {
    this.seenVersions = seenVersions;
  }

  getAllOfVersionsOfSeenGuides() {
    return Promise.resolve(this.seenVersions);
  }

  saveVersionsOfSeenGuides(versions) {
    this.seenVersions.push(...versions);
  }
}
