import React from 'react';

export default class MockContainer extends React.Component {
  render() {
    return <h1>Mock container</h1>;
  }
}
